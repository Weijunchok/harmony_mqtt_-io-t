#ifndef __DHT11_H__
#define __DHT11_H__

void dht_test(void);
void dht_task(void);
uint8 DHT11_ReadData(uint8 *h);
uint8 DHT11_Init(void);
uint8 DHT11_Check(void);
void DHT11_RST (void);
void DHT11_IN(void);
#endif /* __DHT11_H__ */