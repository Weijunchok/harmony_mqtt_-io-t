/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "mmled.h"
#include <stdio.h>
#include <unistd.h>
#include "wifiiot_gpio.h"
#include "wifiiot_gpio_ex.h"
#include "cmsis_os2.h"
 
// LED状态
enum LedState {
    LED_ON = 0,
    LED_OFF,
    LED_SPARK,
};
 
enum LedState g_ledState = LED_SPARK;
 
// LED初始化
void Led_Init(void){
    GpioInit();
    IoSetFunc(WIFI_IOT_IO_NAME_GPIO_9, WIFI_IOT_IO_FUNC_GPIO_9_GPIO);
    GpioSetDir(WIFI_IOT_IO_NAME_GPIO_9, WIFI_IOT_GPIO_DIR_OUT);
}
void Led_OFF(void){
    GpioSetOutputVal(WIFI_IOT_IO_NAME_GPIO_9, 0);
}
void Led_ON(void){
    GpioSetOutputVal(WIFI_IOT_IO_NAME_GPIO_9, 1);
} 
static void *LedBTask(const char *arg){
    (void)arg;
    while (1) {
        switch (g_ledState) {
            case LED_ON:
                GpioSetOutputVal(WIFI_IOT_IO_NAME_GPIO_9, 1);
                usleep(LED_INTERVAL_TIME_US);
                break;
            case LED_OFF:
                GpioSetOutputVal(WIFI_IOT_IO_NAME_GPIO_9, 0);
                usleep(LED_INTERVAL_TIME_US);
                break;
            case LED_SPARK:
                GpioSetOutputVal(WIFI_IOT_IO_NAME_GPIO_9, 0);
                usleep(LED_INTERVAL_TIME_US);
                GpioSetOutputVal(WIFI_IOT_IO_NAME_GPIO_9, 1);
                usleep(LED_INTERVAL_TIME_US);
                break;
            default:
                usleep(LED_INTERVAL_TIME_US);
                break;
        }
    }
 
    return NULL;
}
 
// LED模块初始化线程
void Start_Led_Thread(void){
    osThreadAttr_t ledattr;
    ledattr.name = "LedBTask";
    ledattr.attr_bits = 0U;
    ledattr.cb_mem = NULL;
    ledattr.cb_size = 0U;
    ledattr.stack_mem = NULL;
    ledattr.stack_size = LED_TASK_STACK_SIZE;
    ledattr.priority = LED_TASK_PRIO;
 
    if (osThreadNew((osThreadFunc_t)LedBTask, NULL, &ledattr) == NULL) {
        printf("[LedExample] Falied to create LedTask!\n");
    }
}