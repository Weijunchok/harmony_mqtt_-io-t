#ifndef MMLED_H
#define MMLED_H
 
#define LED_INTERVAL_TIME_US 3000000
#define LED_TASK_STACK_SIZE 512
#define LED_TASK_PRIO 25
 
/**
 * LED初始化
 */
void Led_Init(void);
 
void Start_Led_Thread(void);
void Led_ON(void);
void Led_OFF(void); 
//static void *LedBTask(const char *arg);
 
#endif