/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <stdio.h>
#include "ohos_init.h"
#include "ohos_types.h"
#include "mmled.h"
#include "wifi_connect_demo.h" 
#include "dht11.h"
#include "oled_demo.h"
#include "unistd.h"
#include "mqtttest.h"
#include "hi_time.h"
#include "cmsis_os2.h"
//#include "spitest.h"

void HelloWorld(void)
{   
    printf("[DEMO] Hello world.\n");
    Led_Init();
    //Start_Led_Thread();
    OledDemo();
    WifiConnectDemo();
    mqtt_Thread();
    mqttrec_Thread();
    dht_test();

    //osDelay(1000*1000);
    //my_task();
    //需要定时发心跳，需要将my_task延后

    
    //spi_test();
    

}

 
SYS_RUN(HelloWorld);